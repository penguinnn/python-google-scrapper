# Google Scrapper

Scrapping script for google search results utilising Selenium framework and written in Python

## Usage

Run ./googleScrapper.py searchStrings.txt
The provided text file must contain one search string per line

### Optional Arguments

    -l - Specify log file location e.g. -l output/log.txt . The default location is ./out/googleScrapper.log
    -o - Specify output folder location e.g. -o output/ . The default location is ./out/
    -r - Specify limit of records to be scrapped per search string
    -h - Run web driver in headless mode
    -m - Multiple output mode. Writes output for search strings in a seperate file.
### Examples

    ./googleScrapper.py queries.txt
    ./googleScrapper.py -l result/scrapper.log -h -o result/ -r 50 -m queries.txt

## Output

The script writes all scrapped links to one CSV file named googleScrapper.csv in the following format:

    UID,ID,searchString,Address
    1,1,example,example.org
    2,2,example,example.net
    3,1,info,info.net
    4,2,info,info.org

In multiple output mode scrapped links are written in seperate CSV files, named after the search string and ending with .csv, in the following format:

    ID,Address
    1,example.org
    2,example.net

In both cases the script will truncate previous entries if the output files already exist.

from types import NoneType
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.firefox import GeckoDriverManager
from pathlib import Path
import sys
import time
from datetime import datetime

def warnexit(errors, message):
    print(str(datetime.now()) + "\tCritical error\t" + message)
    driver.quit()
    sys.exit(errors)

class record:
    def __init__(self, UID, ID, SearchString, address):
        self.UID = UID
        self.ID = ID
        self.SearchString = SearchString
        self.address = address

if len(sys.argv) < 2:
    warnexit(1, "Input not provided")
try:
    searchStrings = open(sys.argv[len(sys.argv)-1], 'r')
except Exception as e:
    warnexit(1, "Input file cannot be opened - " + str(e))

try:
    outdir = Path(sys.argv[sys.argv.index('-o')+1])
except:
    print("Using default path for output")
    outdir = Path("out/")
finally:
    try:
        outdir.mkdir(parents = True, exist_ok = True)
    except Exception as e:
        warnexit(1, "Cannot create output directory - " + str(e))
    print("Outputting results in " + outdir.name)

try:
    logdir = Path(sys.argv[sys.argv.index('-l')+1])
except:
    print("Using default path for log")
    logdir = Path("out/googleScrapper.log")
finally:
    try:
        logdir.parent.mkdir(parents = True, exist_ok = True)
    except Exception as e:
        warnexit(1, "Cannot create log directory - " + str(e))
    print("Logging to " + logdir.parent.name+ "/" + logdir.name)
try:
    log = logdir.open("a", encoding = "utf-8 ")
except Exception as e:
    warnexit(1, "Problem opening or creating log file - " + str(e))

log.write(str(datetime.now())  + "\tStarted scrapping\n")

try:
    multimode = sys.argv.index('-m') != 0
except:
    multimode = False
options = Options()
try:
    options.headless = sys.argv.index('-h') != 0
except:
    options.headless = False
try:
    recordlimit = sys.argv[sys.argv.index('-r')+1]
    try:
        recordlimit = int(recordlimit)
    except:
        warnexit(1, "Invalid record limit argument")
except:
    recordlimit = 0
errors = 0
uid = 1
options.add_argument("--window-size=1920,1080")
driver = webdriver.Chrome(options=options, service=Service(GeckoDriverManager().install()))
searchUrl = "https://www.google.com/search?q="

def search(query, errors, uid):
    id = 1
    result = dict()
    previous = ""
    page = 1
    result["uid"] = uid
    result["errors"] = errors
    result["records"] = list()
    try:
        driver.get(searchUrl+query)
        print("Page " + str(page) + " for " + query)
    except Exception as e:
        log.write(str(datetime.now())  + "\tCannot GET" + searchUrl + query + " - " + str(e) + "\n")
        result["errors"] += 1
        return result
    while True:
        try:
            results = driver.find_element(By.ID,'res').find_elements(By.TAG_NAME, 'a')
        except Exception as e:
            log.write(str(datetime.now())  + "\tProblem reading results for " + query + " - " + str(e) + "\n")
            result["errors"] +=1
            return result
        for x in results:
            address = x.get_attribute('href')
            if type(address) == NoneType:
                continue
            if "google.com" not in address and "googleusercontent.com" not in address and address != previous:
                result["records"].append(record(result["uid"], str(id), query, address))
                if id >= recordlimit:
                    return result
                id += 1
                result["uid"] += 1
                previous = address
        time.sleep(4)
        try:
            nextButton = driver.find_element(By.ID, 'pnnext')
            nextButton.click()
        except:
            print("Last page for " + query + " or robot check reached")
            return result
        page +=1
        print("Page " + str(page) + " for " + query)
  
def writeRecords(records, errors):
    if len(records) > 0:
        try:
            outfile = None
            if multimode:
                outfile = Path(outdir / str(records[0].SearchString + ".csv")).open("w", encoding= "utf-8")
            else:
                outfile = Path(outdir / "googleScrapper.csv").open("a", encoding= "utf-8")
        except Exception as e:
            log.write(str(datetime.now())  + "\tProblem opening or creating output file while working on " + records[0].SearchString + str(e) + "\n")
            errors += 1
            warnexit(errors, "\tProblem opening or creating output file while working on " + records[0].SearchString + str(e))
        result = ""
        if multimode:
            result = "ID,Address\n" 
        for x in records:
            if multimode:
                result += str(x.ID) + "," + x.address + "\n"
            else:
                result += str(x.UID) + "," + str(x.ID) + "," + x.SearchString + "," + x.address + "\n"
        try:
            outfile.write(result)
        except Exception as e:
            log.write(str(datetime.now())  + "\tProblem writing to output file for " + query + str(e) + "\n")
            errors += 1
            warnexit(errors, "\tProblem writing to output file for " + query + str(e))
        try:
            outfile.close()
        except Exception as e:
            log.write(str(datetime.now())  + "\tProblem closing output file after writing results for " + query + str(e) + "\n")
            errors += 1
            warnexit(errors, "\tProblem closing output file after writing results for " + query + str(e))

#header for single CSV
if not multimode:
    try:
        outfile = Path(outdir / "googleScrapper.csv").open("w+", encoding= "utf-8").write("UID,ID,searchString,Address\n")
    except Exception as e:
        log.write(str(datetime.now())  + "\tProblem opening or creating output file" + str(e) + "\n")
        errors += 1
        warnexit(errors, "\tProblem opening or creating output file" + str(e))

for query in searchStrings.readlines():
    query = str(query).strip("\n").strip("\r")
    temp = search(query, errors, uid)
    errors = temp["errors"]
    uid = temp["uid"]
    writeRecords(temp["records"], errors)
    print("Done with " + query)

if errors > 0:
    print("Finished with non-critical errors. See log.")
else:
    print("Finished without errors.")
log.write(str(datetime.now())  + "\tFinished scrapping\n")
driver.quit()
sys.exit(errors)
